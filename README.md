# About
 * Simple clips suited for low cost PIR module
 * Include 3D models created in
   [FreeCad](https://www.freecadweb.org)

# Preview
 ![Table light](doc/img/table_light_3D.png)
 ![Table light real](doc/img/table_light_real.jpg)
 ![Shelf light](doc/img/shelf_holder_3D.png)
 ![Shelf light real](doc/img/shelf_holder_real.jpg)
