# Design flow
 * Due to current consumption of MCU, it is better to activate whole logic
   by PIR's output signal.
 * Due to quite "high" MCU consumption in active mode (nearly same as 2 LEDs),
   MCU is not good replacement as kill switch when there is light (not
   energetically efficient solution)
 * LED can not be used as light detector due to very low voltage (8mV when
   "light") and if there is some load, there is less than 1mV -> under noise
   level.
 * On `BISS0001` pin 9 can be used for light detection. It leaks at about
   `20 uA`. If voltage on that pin is higher than `0.2 V`, PIR is disabled.
   Photoresistor can be usually connected between this pin (RL) and GND.
   Possible tuning via serial or paralel resistor.
 * At `RL` pins is connected photoresistor with `47k` resistor (parallel) so
   light will turn on when light intensity is lower. However due to instability
   of whole PIR module, it is not possible to tune it to "close to dark"
   threshold.

# Current consumption
## AVR
 * `800 uA` active mode
 * Loaded FW from "battery charger" project (watching ADC value and on/off IO).
 * According to datasheet and deep sleep with watchdog it consume at about
   `10 uA`.
## PIR
 * `50 uA` constantly (regardless on on/off output)
 * Floating output signal - no load
## LED
 * `600 uA`
 * Connected between GND and PIR output signal (limited resistor on PIR PCB)

